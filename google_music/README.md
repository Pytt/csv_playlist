If you have 2-step verification enabled in your Google account you need to create an application password for the script, see [https://support.google.com/accounts/answer/185833](https://support.google.com/accounts/answer/185833). You can use any application name when creating the password.

If you are having problems getting the script to log into your account enabling 2-step verification and/or ["allowing less secure apps"](https://support.google.com/accounts/answer/6010255) can sometimes help.
