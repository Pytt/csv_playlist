#!/usr/bin/env python3

import argparse
import csv
import os
from getpass import getpass

from gmusicapi.clients import Mobileclient, Webclient


def main(email, password, csv_dir_path):
    client = Mobileclient()
    logged_in = client.login(email, password, client.FROM_MAC_ADDRESS)
    if not logged_in:
        print('Can\'t log in with specified credentials.')
        return
    for file_name in os.listdir(csv_dir_path):
        playlist_name, ext = os.path.splitext(file_name)
        file_path = os.path.join(csv_dir_path, file_name)
        if os.path.isfile(file_path) and ext == '.csv':
            with open(file_path) as csv_file:
                reader = csv.reader(csv_file, delimiter=',',
                                    quotechar='|', quoting=csv.QUOTE_MINIMAL)
                create_playlist(playlist_name, reader, client)
                print('Playlist "{}" has been created'.format(playlist_name))


def create_playlist(name, csv_reader, client):
    song_ids = []
    for row in csv_reader:
        song_query = ' '.join(row)
        search_res = client.search(song_query)
        for song in search_res['song_hits']:
            if song['track']['trackAvailableForSubscription']:
                song_ids.append(song['track']['storeId'])
                break
    playlist_id = client.create_playlist(name)
    client.add_songs_to_playlist(playlist_id, song_ids)


if __name__ == '__main__':
    parser = argparse.ArgumentParser('Uploads all CSV playlists in a directory to Google Music')
    parser.add_argument('google_email')
    parser.add_argument('directory')
    args = parser.parse_args()
    password = getpass()
    main(args.google_email, password, args.directory)
